using GymWpf.Models;
using NUnit.Framework;
using System.Threading;

namespace UnitTests;

public class GymTest
{
    public readonly Gym _gym = Gym.GetInstance(null);

    [Test]
    public void TestCurrentCapIsAlwaysLowerThanMAXCAP()
    {
        for(int i=1;i<=10;i++)
        {
            User user = new User
            {
                Id = i,
                Name = $"user {i}"
            };
            _gym.Enter(user);
        }
        Thread.Sleep(3000);
        Assert.IsTrue(_gym.CurrentCap <= _gym.MAXCAP && _gym.CurrentCap>0);
    }
}
