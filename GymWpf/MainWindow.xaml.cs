﻿using GymWpf.ViewModels;
using System.Windows;

namespace GymWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private GymViewModel _gymViewModel;
        public MainWindow()
        {
            _gymViewModel = new  GymViewModel();
            this.DataContext = _gymViewModel;
            InitializeComponent();
        }

        private void AddPlayer(object sender, RoutedEventArgs e)
        {
            _gymViewModel.AddPlayer();
        }
    }
}
