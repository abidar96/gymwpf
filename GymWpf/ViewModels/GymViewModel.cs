﻿using GymWpf.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;

namespace GymWpf.ViewModels;

public class GymViewModel : INotifyPropertyChanged
{
    private readonly Gym _gym;

    public event PropertyChangedEventHandler PropertyChanged;

    private static int idGen = 1;

    private int _currentCap;
    public int CurrentCap { 
        get
        {
            return _currentCap;
        }
        set
        {
            _currentCap = value;
            OnPropertyRaised(nameof(CurrentCap));
        }
    }

    private ObservableCollection<string> _messages;

    public ObservableCollection<string> Messages
    {
        get
        {
            return _messages;
        }
        set
        {
            _messages = value;
            OnPropertyRaised(nameof(Messages));
        }
    }

    private int _queuCounter;
    public int QueuCounter 
    {
        get
        {
            return _queuCounter;
        }
        set
        {
            _queuCounter = value;
            OnPropertyRaised(nameof(QueuCounter));
        }
    }

    public GymViewModel()
    {
        PropertyChangedEventHandler prop = (s, e) =>
        {
            if (s is Gym ifout)
            {
                CurrentCap = ifout.CurrentCap;
                QueuCounter = ifout.QueueCounter;
                Messages = ifout.Messages;
            }
        };
        _gym = Gym.GetInstance(prop);
        CurrentCap = _gym.CurrentCap;
        QueuCounter = _gym.QueueCounter;
        Messages = new ObservableCollection<string>();
    }

    private void OnPropertyRaised(string propertyname)
    {
        if (PropertyChanged != null)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(propertyname));
        }
    }

    public void AddPlayer()
    {
        var id = idGen++;
        User user = new User()
        {
            Id = id,
            Name = "Ali " + id,
        };
        _gym.Enter(user);
    }

}
