﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace GymWpf.Models;

public class Gym : INotifyPropertyChanged
{
    private static Gym _instance { get; set; }

    public readonly int MAXCAP = 5;

    private readonly AutoResetEvent auto = new AutoResetEvent(true);

    private ConcurrentQueue<User> _queue;

    private readonly List<Task> _tasks = new List<Task>();

    private ObservableCollection<string> _messages;

    public ObservableCollection<string> Messages
    {
        get{return _messages;}

        set
        {
            _messages = value;
            OnPropertyChanged(nameof(Messages));
        }
    }

    private int _currentCap = 0;
    public int CurrentCap {
        get { return _currentCap; } 
        set {
            _currentCap = value;
            OnPropertyChanged(nameof(CurrentCap));
        }
    }

    private int _queueCounter = 0;
    public int QueueCounter
    {
        get { return _queueCounter; }
        set
        {
            _queueCounter = value;
            OnPropertyChanged(nameof(QueueCounter));
        }
    }

    private bool _isRunning;

    public event PropertyChangedEventHandler PropertyChanged;

    private Gym() { }

    public static Gym GetInstance(PropertyChangedEventHandler propertyChangedEventHandler)
    {
        if( _instance == null)
        {
            _instance = new Gym();
            _instance.CurrentCap = 0;
            _instance._queue = new ConcurrentQueue<User>();
            _instance._isRunning = false;
            _instance.PropertyChanged = propertyChangedEventHandler;
            _instance._messages = new ObservableCollection<string>();
        }
        
        return _instance;
    }

    protected void OnPropertyChanged([CallerMemberName] string name = null)
    {
        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
    }

    public void Enter(User user)
    {
        _queue.Enqueue(user);
        QueueCounter= _queue.Count;
        if (!_isRunning)
            Task.Run(() => Run());
        else
            auto.Set();
    }

    public void Run()
    {
        _isRunning = true;
        while (_isRunning)
        {
            if (_tasks.Count > MAXCAP)
                throw new IndexOutOfRangeException($"The gym cap can't be greater than MaxCAP = {MAXCAP}");

            if (_tasks.Count >= MAXCAP)
            {
                Task.WaitAny(_tasks.ToArray());    
            }

            _tasks.RemoveAll(task => task.IsCompleted);
            CurrentCap = _tasks.Count;
            while (_tasks.Count < MAXCAP && _queue.Count > 0)
            {
                if (_queue.TryDequeue(out var user))
                {
                    _tasks.Add(Task.Run(() => DoWork(user)));
                }    
            }
            CurrentCap = _tasks.Count;
            if (_queue.IsEmpty && _tasks.Count == 0)
                auto.WaitOne();
        }
    }

    public void DoWork(User user)
    {
        QueueCounter = _queue.Count;
        Train(user);

        while (_queue.TryDequeue(out var user2))
        {
            QueueCounter = _queue.Count;
            Train(user2);
        }
        auto.Set();
    }

    private void Train(User user)
    {
        App.Current.Dispatcher.BeginInvoke(() =>
        {
            Messages.Add($"{user.Name} start training");
        });
        Debug.WriteLine($"{user.Name} start training");

        Thread.Sleep(10000);

        App.Current.Dispatcher.BeginInvoke(() =>
        {
            Messages.Add($"{user.Name} finished training {System.DateTime.Now}");
        });
        Debug.WriteLine($"{user.Name} finished training");
    }
    
}
